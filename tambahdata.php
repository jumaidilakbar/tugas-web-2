<?php
 include('koneksi.php'); //agar index terhubung dengan database, maka koneksi sebagai penghubung harus di include 
?>
<!DOCTYPE html>
<html>
 <head>
 <title>CRUD Produk dengan gambar - Mahsiswa UMMY SOLOK 2022</title>
 <style type="text/css">
 * {
            font-family: "Trebuchet MS";
            }
            h1 {
            text-transform: uppercase;
            color: salmon;
            }
            button {
            background-color: salmon;
            color: #fff;
            padding: 10px;
            text-decoration: none;
            font-size: 12px;
            border: 0px;
            margin-top: 20px;
            }
            label {
            margin-top: 10px;
            float: left;
            text-align: left;
            width: 100%;
            }
            input {
            padding: 6px;
            width: 100%;
            box-sizing: border-box;
            background: #f8f8f8;
            border: 2px solid #ccc;
            outline-color: salmon;
            }
            div {
            width: 100%;
            height: auto;
            }
            .base {
            width: 400px;
            height: auto;
            padding: 20px;
            margin-left: auto;
            margin-right: auto;
            background: #ededed;
}
 </style>
 </head>
 <body>
 <center>
 <h1>Tambah Biodata Mahasiswa</h1>
 <center>
 <form method="POST" action="proses_tambah.php" enctype="multipart/form-data" >
 <section class="base">
 <div>
 <label>NIM</label>
 <input type="text" name="NIM" autofocus="" required="" />
 </div>
 <div>
 <label>Nama Mahasiswa</label>
 <input type="text" name="nama_mhs" autofocus="" required="" />
 </div>
  <div>
 <label>Tempat, Tanggal Lahir</label>
 <input type="text" name="ttl" />
 </div>
 <div>
 <label>Alamat</label>
 <input type="text" name="alamat" required="" />
 </div>
 <div>
 <label>Agama</label>
 <input type="text" name="agama" required="" />
 </div>
 <div>
 <label>Jurusan</label>
 <input type="text" name="jurusan" required="" />
</div>
 <div>
 <label>Input Foto</label>
 <input type="file" name="foto" required="" />
 </div>
 <div>
 <button type="submit">Simpan Biodata</button>
 </div>
 </section>
 </form>
 </body>
</html>
