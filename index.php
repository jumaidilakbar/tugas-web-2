<?php
 include('koneksi.php'); //*agar index terhubung dengan database, maka koneksi sebagai penghubung harus di include
 
?>
<!DOCTYPE html>
<html>
 <head>
 <title>CRUD Data Akademik</title>
 <style type="text/css">
 * {
        font-family: "Trebuchet MS";
        }
        h1 {
        text-transform: uppercase;
        color: salmon;
        }
        table {
        border: solid 1px #DDEEEE;
        border-collapse: collapse;
        border-spacing: 0;
        width: 80%;
        margin: 10px auto 10px auto;
        }
        table thead th {
        background-color: #DDEFEF;
        border: solid 1px #DDEEEE;
        color: #336B6B;
        padding: 10px;
        text-align: left;
        text-shadow: 1px 1px 1px #fff;
        text-decoration: none;
        }
        table tbody td {
        border: solid 1px #DDEEEE;
        color: #333;
        padding: 10px;
        text-shadow: 1px 1px 1px #fff;
        }
        a {
        background-color: salmon;
        color: #fff;
        padding: 10px;
        text-decoration: none;
        font-size: 12px;
 }
 </style>
 </head>
 <body>
 <center><h1>Data Mahasiswa</h1><center>
 <center><a href="tambahdata.php">+ &nbsp; Tambah Data mahasiswa</a><center>
 <br/>
 <table>
 <thead>
 <tr>
 <th>No</th>
 <th><center>NIM</center></th>
 <th>nama</th>
 <th>Tempat Tanggal Lahir</th>
 <th>Alamat</th>
 <th>Agama</th>
 <th>Jurusan</th>
 <th>Gambar</th>
 <th>Action</th>
 </tr>
 </thead>
 <tbody>
 <?php
 // jalankan query untuk menampilkan semua data diurutkan berdasarkan nim
 $query = "SELECT * FROM mahasiswa ORDER BY id ASC";
 $result = mysqli_query($koneksi, $query);
 //mengecek apakah ada error ketika menjalankan query

 if(!$result){
 die ("Query Error: ".mysqli_errno($koneksi).
 " - ".mysqli_error($koneksi));
 }
 //buat perulangan untuk element tabel dari data mahasiswa
 $no = 1; //variabel untuk membuat nomor urut
 // hasil query akan disimpan dalam variabel $data dalam bentuk array
 // kemudian dicetak dengan perulangan while
 while($row = mysqli_fetch_assoc($result))
 {
 ?>
 <tr>
 <td><?php echo $no; ?></td>
 <td><?php echo $row['NIM']; ?></td>
 <td><?php echo $row['nama_mhs']; ?></td>
 <td><?php echo $row['ttl']; ?></td>
 <td><?php echo $row['alamat']; ?></td>
 <td><?php echo $row['agama'];?></td>
 <td><?php echo $row['jurusan']; ?></td>
 <td style="text-align: center;"><img src="gambar/<?php echo $row['foto']; ?>" style="width: 120px;"></td>
 <td>
 <a href="view_edit.php?id=<?php echo $row['id']; ?>">Edit</a>|<a href="proses_hapus.php?id=<?php echo $row['id']; ?>"onclick="return confirm('Anda Yakin Akan menghapus Data Ini?')">Hapus</a>
 </td>
 </tr>
 
 <?php
 $no++; //untuk nomor urut terus bertambah 1
 }
 ?>
 </tbody>
 </table>
 </body>
</html>



<?php
 include('koneksi.php'); //*agar index terhubung dengan database, maka koneksi sebagai penghubung harus di include
 
?>
<!DOCTYPE html>
<html>
 <head>
 <title>CRUD Data Akademik</title>
 <style type="text/css">
* {
 font-family: "Trebuchet MS";
 }
 h1 {
 text-transform: uppercase;
 color: salmon;
 }
 table {
 border: solid 1px #DDEEEE;
 border-collapse: collapse;
 border-spacing: 0;
 width: 80%;
 margin: 10px auto 10px auto;
 }
 table thead th {
 background-color: #DDEFEF;
 border: solid 1px #DDEEEE;
 color: #336B6B;
 padding: 10px;
 text-align: left;
 text-shadow: 1px 1px 1px #fff;
 text-decoration: none;
 }
 table tbody td {
 border: solid 1px #DDEEEE;
 color: #333;
 padding: 10px;
 text-shadow: 1px 1px 1px #fff;
 }
 a {
 background-color: salmon;
 color: #fff;
 padding: 10px;
 text-decoration: none;
 font-size: 12px;
 }
 </style>
 </head>
 <body>
 <center><h1>Data Matakuliah</h1><center>
 <center><a href="tambahdata_matkul.php">+ &nbsp; Tambah Data Matakuliah</a><center>
 <br/>
 <table>
 <thead>
 <tr>
 <th>No</th>
 <th><center>kode matkul</center></th>
 <th>matkul</th>
 <th>sks</th>
 <th>Jadwal</th>
 <th>jam mulai</th>
 <th>jam berakhir</th>
 <th>dosen</th>
 <th>Action</th>
 </tr>
 </thead>
 <tbody>
 <?php
 // jalankan query untuk menampilkan semua data diurutkan berdasarkan nim
 $query = "SELECT * FROM matkul ORDER BY id_matkul ASC";
 $result = mysqli_query($koneksi, $query);
 //mengecek apakah ada error ketika menjalankan query

 if(!$result){
 die ("Query Error: ".mysqli_errno($koneksi)." - ".mysqli_error($koneksi));
 }
 //buat perulangan untuk element tabel dari data mahasiswa
 $no = 1; //variabel untuk membuat nomor urut
 // hasil query akan disimpan dalam variabel $data dalam bentuk array
 // kemudian dicetak dengan perulangan while
 while($row = mysqli_fetch_assoc($result))
 {
 ?>
 <tr>
 <td><?php echo $no; ?></td>
 <td><?php echo $row['kd_matkul']; ?></td>
 <td><?php echo $row['nama_matkul']; ?></td>
 <td><?php echo $row['sks']; ?></td>
 <td><?php echo $row['jadwal']; ?></td>
 <td><?php echo $row['mulai'];?></td>
 <td><?php echo $row['berakhir']; ?></td>
 <td><?php echo $row['dosen']; ?></td>
 <td>
 <a href="view_edit matkul.php?id_matkul=<?php echo $row['id_matkul']; ?>">Edit</a>|<a href="hapus_matkul.php?id_matkul=<?php echo $row['id_matkul']; ?>" onclick="return confirm('Anda yakin akan menghapus data ini?')">Hapus</a>
 </td>
 </tr>
 
 <?php
 $no++; //untuk nomor urut terus bertambah 1
 }
 ?>
 </tbody>
 </table>
 </body>
</html>